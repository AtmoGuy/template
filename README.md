# Project Name

Project template for projects that get things done.

## Installation
Clone this repository

## Dependencies
* Dep. 1
* Dep. 2
* Dep. 3

## Usage
```python
import package as pkg

pkg.foo(bar)
```

## Notes
Last worked on line XX of foo bar.
Need to run foobar.py before barfoo.py

## Contributions
Pull requests are/are not welcome. If you want to make a major contribution, please open an issue first for discussion.